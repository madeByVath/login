//Angular code
(function () {
    angular.module('loginApp').controller("UserController", ['$http', '$scope', '$window', '$cookies', 'accessService', 'userConnected','$timeout',  function ($http, $scope, $window, $cookies, accessService, userConnected, $timeout) {

            //scope variables
            // method singelton to create user
            if (userConnected.user != undefined) {
                $scope.user = userConnected.user;
            } else {
                $scope.user = new User();
            }

            $scope.userOption = 0;
            $scope.passwordValid = true;
            $scope.nickValid = true;

              
            /**
             * connection
             * make a ajax call to test login data of ok return user data
             * @returns {undefined}
             */
            this.connection = function () {
                //copy
                $scope.user = angular.copy($scope.user);
                //Server conenction to verify user's data
                var promise = accessService.getData("php/controllers/MainController.php", 
                    true, "POST", {controllerType: 0, action: 10000, jsonData: JSON.stringify($scope.user)});
                promise.then(function (outPutData) {
                    if (outPutData[0] === true) {
                        //We get users data
                        // In outPutData[1] we have an array of only one position
                        // outPutData[1][0] is a pseudo object
                        var user = new User();
                        user.construct(outPutData[1][0].id, outPutData[1][0].nick, 
                                    outPutData[1][0].password);
                        console.log(user);
                        $scope.userOption = 0;
                        $scope.error = 0;
                        sessionStorage.setItem("connectedUser", JSON.stringify(outPutData[1][0]));
                        window.open("mainWindow.html", "_self");
                    } else {
                        if (angular.isArray(outPutData[1])) {
                            $scope.error = 1;
                            showErrors(outPutData[1]);
                        } else {
                            alert("There has been an error in the server, try later");
                        }
                    }
                });
            }; // END connection
            
        }]); // END controller

})(); // END function

