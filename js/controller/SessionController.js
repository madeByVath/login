//Angular code
(function () {
    angular.module('loginApp').controller("SessionController", ['$http', '$scope', '$window', '$cookies', 'accessService', 'userConnected', '$timeout',  function ($http, $scope, $window, $cookies, accessService, userConnected, $timeout) {



            //scope variables
            $scope.user = new User();
            $scope.userAction = 0;

            // clock and countdown to logout
            $scope.clock = "loading clock..."; // initialise the time variable
                $scope.tickInterval = 1000; //ms
                $scope.countdown = 10;
                var tick = function() {
                    $scope.clock = Date.now(); // get the current time
                    $timeout(tick, $scope.tickInterval); // reset the timer
                    $scope.countdown=$scope.countdown-1;
                    if($scope.countdown < 1){
                        $scope.logOut();
                    }
                };
                // Start the timer
                $timeout(tick, $scope.tickInterval);
            
            

            /**
             * sessionControl
             * make a ajax call to take conected user
             * @returns {undefined}
             */
            this.sessionControl = function () {
                switch ($scope.userAction) {
                    //Index.html is executed
                    case 0:
                        if (typeof (Storage) == "undefined") {
                            alert("Your browser is not compatible with sessions, upgrade your browser");
                        } else {
                            if (sessionStorage.length > 0) {
                                var objAux = JSON.parse(sessionStorage.getItem("connectedUser"));
                                var user = new User();
                                user.construct(objAux.id, 
                                            objAux.nick, 
                                            objAux.password);
                                console.log(user);
                                if (!isNaN(user.getId())) {
                                    userConnected.user = user;
                                    window.open("mainWindow.html", "_self");
                                }
                            }
                            var promise = accessService.getData("php/controllers/MainController.php", 
                            true, "GET", {controllerType: 0, action: 10030, jsonData: {none: ""}});
                            promise.then(function (outPutData) {
                                if (outPutData[0] === true) {
                                    if (typeof (Storage) == "undefined") {
                                        alert("Your browser is not compatible with sessions, upgrade your browser");
                                    } else {
                                        console.log(user);
                                        $scope.userManag = 0;
                                        sessionStorage.setItem("connectedUser", JSON.stringify(outPutData[1]));
                                        userConnected.user = user;
                                        window.open("mainWindow.html", "_self");
                                    }
                                } else {
                                    if (!angular.isArray(outPutData[1])) {
                                        alert("There has been an error in the server, try later");
                                    }
                                }
                            });
                        }
                        break;
                        //mainWindow.html is executed
                    case 1:
                        if (typeof (Storage) == "undefined") {
                            //El navegador és compatible LocalStorage;
                            alert("This browser does not accept local sessions management");
                        } else {
                            //~ if (sessionStorage.length > 0) $scope.user.fromStorage(sessionStorage.getItem('user'));
                            if (sessionStorage.length > 0) {
                                var obj = JSON.parse(sessionStorage.getItem('connectedUser'));
                                $scope.user.construct(obj.id, 
                                                    obj.nick, 
                                                    obj.password);
                                $scope.passControl = $scope.user.getPassword();
                                $scope.sessionOpened = true;
                            }
                            if (isNaN($scope.user.getId())) {
                                window.open("index.html", "_self");
                            } else {
                                userConnected.user = $scope.user;
                            }
                        }
                        break;
                    default:
                        console.log("user action not correcte: " + $scope.userAction);
                        break;
                } // END switch
            }; // sessionControl

            /**
             * logOut
             * logOut session
             * @returns {undefined}
             */
            $scope.logOut = function () {
                //Local session destroy
                if (typeof (Storage) == "undefined") {
                    //El navegador és compatible LocalStorage;
                    alert("This browser does not accept local sessions management");
                } else {
                    sessionStorage.removeItem('connectedUser');
                    //Server session destroy
                    var promise = accessService.getData("php/controllers/MainController.php", 
                        true, "POST", {controllerType: 0, action: 10040, jsonData: {none: ""}});
                    promise.then(function (outPutData) {
                        if (outPutData[0] === true) {
                            window.open("index.html", "_self");
                        } else {
                            if (angular.isArray(outPutData[1])) {
                                showErrors(outPutData[1]);
                            } else {
                                alert("There has been an error in the server, try later");
                            }
                        }
                    });
                }
            }; // END logOut
        }]);
})();
