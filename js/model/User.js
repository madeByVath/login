function User() {
    // Attributs
    this.id;
    this.nick;
    this.password;

    // Contructor
    this.construct = function (id, nick, password) {
        this.setId(id);
        this.setNick(nick);
        this.setPassword(password);
    };

    // Setter
    this.setId = function (id) {
        this.id = id;
    };
    this.setNick = function (nick) {
        this.nick = nick;
    };
    this.setPassword = function (password) {
        this.password = password;
    };

    // Getter
    this.getId = function () {
        return this.id;
    };
    this.getNick = function () {
        return this.nick;
    };
    this.getPassword = function () {
        return this.password;
    };


} // END class
