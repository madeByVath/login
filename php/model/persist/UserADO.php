<?php

/** User.php
 * Entity User
 * autor  Christian Vath
 * version 2017/07
 */
require_once "BDConnect.php";
require_once "EntityInterfaceADO.php";
require_once "../model/User.php";

class UserADO implements EntityInterfaceADO {

    //----------Data base Values---------------------------------------
    private static $tableName = "users";
    private static $colNameId = "id";
    private static $colNameNick = "nick";
    private static $colNamePassword = "password";
    
    private static $tableNameLog = "logs";
    private static $colNameLogId = "id";
    private static $colNameLogUserId = "userId";
    private static $colNameLogDate = "logDate";

    //---Databese management section-----------------------
    /**
     * fromResultSetList()
     * this function runs a query and returns an array with all the result transformed into an object
     * @param res query to execute
     * @return objects collection
     */
    public static function fromResultSetList($res) {
        $entityList = array();
        $i = 0;
        //while ( ($row = $res->fetch_array(MYSQLI_BOTH)) != NULL ) {
        foreach ($res as $row) {
            //We get all the values an add into the array
            $entity = UserADO::fromResultSet($row);

            $entityList[$i] = $entity;
            $i++;
        }
        return $entityList;
    }

    /**
     * fromResultSet()
     * the query result is transformed into an object
     * @param res ResultSet del qual obtenir dades
     * @return object
     */
    public static function fromResultSet($res) {
        //We get all the values form the query
        $id = $res[UserADO::$colNameId];
        $nick = $res[UserADO::$colNameNick];
        $password = $res[UserADO::$colNamePassword];

        //Object construction
        $entity = new User();
        $entity->setId($id);
        $entity->setNick($nick);
        $entity->setPassword($password);
        return $entity;
    }

    /**
     * findByQuery()
     * It runs a particular query and returns the result
     * @param cons query to run
     * @return objects collection
     */
    public static function findByQuery($cons, $vector) {
        //Connection with the database
        try {
            $conn = DBConnect::getInstance();
        } catch (PDOException $e) {
            echo "Error executing query.";
            error_log("Error executing query in UserADO: " . $e->getMessage() . " ");
            die();
        }
        $res = $conn->execution($cons, $vector);
        return UserADO::fromResultSetList($res);
    }

    /**
     * findByNickAndPass()
     * It runs a query and returns an object array
     * @param name
     * @return object with the query results
     */
    public static function findByNickAndPass($user) {
        $cons = "select * from `" . UserADO::$tableName . "` where " . 
                UserADO::$colNameNick . " = ? and " . 
                UserADO::$colNamePassword . " = ?";
        $arrayValues = [$user->getNick(), $user->getPassword()];
        return UserADO::findByQuery($cons, $arrayValues);
    }
    
    /**
     * create()
     * insert a new row into the database
     */
    public function saveLog($user) {
        //Connection with the database
        try {
            $conn = DBConnect::getInstance();
        } catch (PDOException $e) {
            print "Error connecting database: " . $e->getMessage() . " ";
            die();
        }
        //$cons = "insert into " . UserADO::$tableNameLog . " (`" . UserADO::$colNameLogUserId ." `,`" . UserADO::$colNameLogDate . "`) values (?, ?)";
        $cons = "insert into " . UserADO::$tableNameLog . " (`userId`,`logDate`) values (?, ?)";
        $arrayValues = [$user->getId(), date("Y-m-d H:i:s")];
        $conn->executionInsert($cons, $arrayValues);
    }

    
}

?>
