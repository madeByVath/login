<?php

interface EntityInterfaceADO {

    public static function fromResultSetList($res);

    public static function fromResultSet($res);

    public static function findByQuery($cons, $vector);

}

?>
