<?php

// Class to connect to database 
class DBConnect {
    private $server;
    private $user;
    private $password;
    private $dataBase;
    private $link;
    private $stmt;
    private $array = array();
    static $_instance;

    // private constructor 
    private function __construct() {
        $this->setConnection();
        $this->connection();
    }

    // method to conect with params to database
    private function setConnection() {
        $this->server = 'localhost';
        $this->dataBase = 'login';
        $this->user = 'root';
        $this->password = 'alumne';
    }

    // Avoid cloning of the object: Singleton patron
    private function __clone() {
        
    }

    /* Function responsible for creating, if applicable, the object.
     * This is the function we have to call from outside the class for
     * Instantiate the object and thus be able to use its methods */
    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    // connection to database 
    private function connection() {
        try {
            $this->link = new PDO('mysql:dbname=' . $this->dataBase . ';host=' . 
                    $this->server, $this->user, $this->password, 
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (PDOException $e) {
            $this->link = null;
            echo "Error connecting to database.";
            error_log("Error connecting to database: " . $e);
        }
    }

    // execute query
    public function execution($sql, $vector) {
        if ($this->link != null) {
            $this->stmt = $this->link->prepare($sql);

            try {
                $this->stmt->execute($vector);
            } catch (PDOException $e) {
                $this->link = null;
                echo "Error executing query.";
                error_log("Error executing query: " . $e);
            }
        } else {
            $this->stmt = null;
        }
        return $this->stmt; //Returns the selected query or the number of rows affected
    }

    //If we need other things, for example, to know the last inserted id, we have to codify it aside
    public function executionInsert($sql, $vector) {
        if ($this->link != null) {
            $this->stmt = $this->link->prepare($sql);
            try {
                $this->stmt->execute($vector);
                $id = $this->link->lastInsertId();
            } catch (PDOException $e) {
                $this->link = null;
                echo "Error executing insert.";
                error_log("Error executing insert: " . $e);
            }
        } else {
            $id = null;
        }
        return $id;
    }

}

?>
