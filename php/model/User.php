<?php

/** userClass.php
 * Entity userClass
 * autor  Christian Vath
 * version 2017/06
 */
require_once "EntityInterface.php";

class User implements EntityInterface {
    // Atributs
    private $id;
    private $nick;
    private $password;

    // Constructor
    function __construct() {
    }

    // Getter
    public function getId() {
        return $this->id;
    }

    public function getNick() {
        return $this->nick;
    }

    public function getPassword() {
        return $this->password;
    }

    // Setter
    public function setId($id) {
        $this->id = $id;
    }
    
    public function setNick($nick) {
        $this->nick = $nick;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    // Get all
    public function getAll() {
        $data = array();
        $data["id"] = $this->id;
        $data["nick"] = $this->nick;
        $data["password"] = $this->password;
        return $data;
    }

    // Set all
    public function setAll($id, $nick, $password) {
        $this->setId($id);
        $this->setNick($nick);
        $this->setPassword($password);
    }


}

?>
