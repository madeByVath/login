<?php

/**
 * toDoClass class
 * it controls the hole server part of the application
 */
require_once "ControllerInterface.php";
require_once "../model/User.php";
require_once "../model/persist/UserADO.php";

class UserControllerClass implements ControllerInterface {
    // Atributs
    private $action;
    private $jsonData;

    // Constructor
    function __construct($action, $jsonData) {
        $this->setAction($action);
        $this->setJsonData($jsonData);
    }

    // Getter
    public function getAction() {
        return $this->action;
    }

    public function getJsonData() {
        return $this->jsonData;
    }

    // Setter
    public function setAction($action) {
        $this->action = $action;
    }

    public function setJsonData($jsonData) {
        $this->jsonData = $jsonData;
    }

    /**
     * doAction
     * switch to the methods
     * @return string
     */
    public function doAction() {
        $outPutData = array();
        switch ($this->getAction()) {
            case 10000:
                $outPutData = $this->userConnection();
                break;
            case 10030:
                $outPutData = $this->sessionControl();
                break;
            case 10040:
                //Closing session
                session_unset();
                session_destroy();
                $outPutData[0] = true;
                break;
            default:
                $errors = array();
                $outPutData[0] = false;
                $errors[] = "Sorry, there has been an error. Try later";
                $outPutData[] = $errors;
                $userView = new UserViewClass($outPutData);
                error_log("Action not correct in UserControllerClass, value: " . $this->getAction());
                break;
        }
        return $outPutData;
    } // END doAction

    /**
     * userConnection
     * test if user and password correct and create session
     * @return type
     */
    private function userConnection() {
        $userObj = json_decode(stripslashes($this->getJsonData()));

        $outPutData = array();
        $errors = array();
        $outPutData[0] = true;
        $user = new User();
        
        $user->setNick($userObj->nick);
        $user->setPassword($userObj->password);
        $userList = userADO::findByNickAndPass($user);
        
        if (count($userList) == 0) {
            $outPutData[0] = false;
            $errors[] = "No user has found with these data";
            $outPutData[1] = $errors;
        } else {
            $usersArray = array();
            foreach ($userList as $user) {
                $usersArray[] = $user->getAll();
                userADO::saveLog($user);
                break;
            }
            $_SESSION['LAST_ACTIVITY'] = time();
            $_SESSION['connectedUser'] = $userList[0];
            $outPutData[1] = $usersArray;
        }
        return $outPutData;
    } // END userConnection

    /**
     * sessionControl
     * test if session exist
     * @return string
     */
    private function sessionControl() {
        $outPutData = array();
        $outPutData[] = true;
        if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 10)) {
                // last request was more than 30 minutes ago
                session_unset();     // unset $_SESSION variable for the run-time 
                session_destroy();   // destroy session data in storage
            }
        if (isset($_SESSION['connectedUser'])) {
            $outPutData[] = $_SESSION['connectedUser']->getAll();
        } else {
            $outPutData[0] = false;
            $errors[] = "No session opened";
            $outPutData[1] = $errors;
        }
        return $outPutData;
    } // END sessionControl
    


} // END UserControllerClass

?>
